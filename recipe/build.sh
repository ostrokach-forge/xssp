#!/bin/sh

set -e

export CPPFLAGS="${CPPFLAGS} -I${PREFIX}/include -Wno-sign-compare"

./autogen.sh
./configure \
    --prefix="$PREFIX" \
    --with-boost="$PREFIX" \
    --with-boost-libdir="$PREFIX/lib"
make
make install
